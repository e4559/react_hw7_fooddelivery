import React, { useRef, useContext } from "react";
import Button from "../ui/Button";
import Checkbox from "../ui/Checkbox";
import InputAccount from "../ui/InputAccount";
import "./Account.css";
import { UserContext } from "../App/App";
import { useNavigate } from "react-router-dom";

const Account = () => {
  const inputRef = useRef<HTMLInputElement | null>(null);
  const x = useContext(UserContext);
  const navigate = useNavigate();
  const handleClick = () => {
    inputRef?.current?.click();
  };
  const handleRemove = () => {
    x?.setUserInfo({ ...x.userInfo, img: x.userInfo.prevImg });
  };
  const handleFileInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files && e.target.files[0];
    file &&
      x?.setUserInfo({
        ...x.userInfo,
        img: URL.createObjectURL(file),
      });
  };
  const handleLogout = () => {
    x?.setIsLogedIn(false);
    navigate("/");
  };

  const firstNameRef = useRef<HTMLInputElement>(null);
  const secondNameRef = useRef<HTMLInputElement>(null);
  const emailRef = useRef<HTMLInputElement>(null);
  const phoneRef = useRef<HTMLInputElement>(null);

  const saveChanges = () => {
    x?.setUserInfo({
      ...x.userInfo,
      email:
        emailRef.current?.value == ""
          ? x.userInfo.email
          : emailRef?.current?.value,
      lastName:
        secondNameRef.current?.value == ""
          ? x.userInfo.lastName
          : secondNameRef?.current?.value,
      firstName:
        firstNameRef.current?.value == ""
          ? x.userInfo.firstName
          : firstNameRef?.current?.value,
      phone:
        phoneRef.current?.value == ""
          ? x.userInfo.phone
          : phoneRef?.current?.value,
    });
  };

  return (
    <div className="account-right-wrapper">
      <p className="acc-p">Account</p>
      <div className="account-item-wrapper">
        <h3>Personal information</h3>
        <p>Avatar</p>

        <div className="img-btn-wrapper">
          <img src={x?.userInfo.img} alt="" />
          <div className="change">
            <Button name="Change" cn="" onClick={handleClick} />
          </div>
          <div className="remove">
            <Button name="Remove" cn="" onClick={handleRemove} />
          </div>
          <input
            type="file"
            ref={inputRef}
            onChange={(e) => handleFileInput(e)}
          />
        </div>

        <form className="account-form">
          <InputAccount
            type="text"
            placeholder={x?.userInfo.firstName}
            label="First name"
            ref1={firstNameRef}
          />
          <InputAccount
            type="text"
            placeholder={x?.userInfo.lastName}
            label="Last name"
            ref1={secondNameRef}
          />
          <InputAccount
            type="email"
            placeholder={x?.userInfo.email}
            label="Email"
            ref1={emailRef}
          />
          <InputAccount
            type="number"
            placeholder={x?.userInfo.phone}
            label="Phone number"
            ref1={phoneRef}
          />
        </form>
        <div className="notifications">
          <h3>Email notifications</h3>
          <form className="notif-form">
            <Checkbox label="New deals" checked={true} />
            <Checkbox label="Password changes" checked={true} />
            <Checkbox label="New restaurants" checked={true} />
            <Checkbox label="Special offers" checked={true} />
            <Checkbox label="Order statuses" checked={true} />
            <Checkbox label="Newsletter" checked={true} />
          </form>
        </div>
        <div className="acc-btns">
          <div className="logout">
            <Button name="Log out" cn="" onClick={handleLogout} />
          </div>
          <div className="save">
            <Button name="Discard changes" cn="discard" />
            <Button name="Save changes" cn="" onClick={saveChanges} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Account;
