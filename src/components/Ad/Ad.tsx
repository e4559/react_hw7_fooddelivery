import React from "react";
import "./Ad.css";

type AdProps = {
  img: string;
  children: React.ReactNode;
  cn: string;
};

const Ad: React.FC<AdProps> = ({ img, children, cn }) => {
  return (
    <div className={cn}>
      <img src={img} alt="add icon" />
      {children}
    </div>
  );
};

export default Ad;
