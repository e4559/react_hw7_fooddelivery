import React, { useState } from "react";
import AccountSetting from "../../pages/AccountSetting";
import { Routes, Route, useNavigate } from "react-router-dom";

import Home from "../../pages/Home";
import Login from "../../pages/Login";
import Error from "../Error";
import userData from "./../../data/users.json";
import Header from "../Header";
import Popup from "../Popup";

type userType = {
  email?: string;
  password: string;
  lastName?: string;
  firstName?: string;
  phone?: string;
  img: string;
  prevImg: string;
};

type dataType = {
  deleteBasketItem: (id: number) => void;
  modalInfo: ModalInfoType;
  errorMessage: string;
  handleLogin: (inputData: { email: string; password: string }) => void;
  isLogedIn: boolean;
  addToBasket1: (id: number, name: string) => void;
  setIsLogedIn: React.Dispatch<React.SetStateAction<boolean>>;
  userInfo: userType;
  setUserInfo: React.Dispatch<React.SetStateAction<userType>>;
};

type ModalInfoType = { name: string; id: number; count: number }[];



const UserContext = React.createContext<dataType | null>(null);

const App = () => {
  const [userInfo, setUserInfo] = useState({} as userType);
  const [isLogedIn, setIsLogedIn] = useState(false);
  const [modalInfo, setModalInfo] = useState<ModalInfoType>([]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const [popup, setPopaup] = useState(true);

  setTimeout(() => {
    setPopaup(false);
  }, 1500);

  const navigate = useNavigate();

  const deleteBasketItem = (id: number) => {
    const filteredInfo = modalInfo.filter((item) => item.id !== id);
    setModalInfo([...filteredInfo]);
  };

  const addToBasket1 = (id: number, name: string) => {
    let includes = false;

    if (!modalInfo.length) {
      setModalInfo([{ name, id, count: 1 }]);
    }
    modalInfo.map((item) => {
      if (item.id == id) {
        item.count += 1;
        includes = true;
      }
      setModalInfo([...modalInfo]);
    });
    if (!includes) {
      setModalInfo([...modalInfo, { id, name, count: 1 }]);
    }
  };
  const handleLogin = (inputData: { [key: string]: string }) => {
    if (
      userData[inputData.email] &&
      userData[inputData.email].password == inputData.password
    ) {
      setUserInfo({
        email: inputData.email,
        password: userData[inputData.email].password,
        lastName: userData[inputData.email].lastName,
        firstName: userData[inputData.email].firstName,
        phone: userData[inputData.email].phone,
        img: userData[inputData.email].img,
        prevImg: userData[inputData.email].img,
      });
      setIsLogedIn(true);
      navigate("home");
    } else {
      setErrorMessage("Invalid Input");
      setTimeout(() => {
        setErrorMessage("");
      }, 1500);
    }
  };
  console.log(popup, "isLogedIn");

  return (
    <div>
      <UserContext.Provider
        value={{
          userInfo,
          setUserInfo,
          setIsLogedIn,
          addToBasket1,
          isLogedIn,
          handleLogin,
          errorMessage,
          modalInfo,
          deleteBasketItem,
        }}
      >
        {isLogedIn ? (
          <>
            <Header
              modalInfo={modalInfo}
              isOpen={isModalOpen}
              setIsOpen={setIsModalOpen}
              img={userInfo.img}
            />

            <Routes>
              <Route index element={<Login />} />
              <Route path="home" element={<Home />} />
              <Route path="account" element={<AccountSetting />} />
              <Route path="*" element={<Error />} />
            </Routes>
          </>
        ) : (
          <>
            {popup && <Popup />}
            <Login />
          </>
        )}
      </UserContext.Provider>
    </div>
  );
};

export { App, UserContext };

/*

{isLogedIn && (
          <Header
            modalInfo={modalInfo}
            isOpen={isModalOpen}
            setIsOpen={setIsModalOpen}
            img={userInfo.img}
          />
        )}

        <Routes>
          <Route index element={<Login />} />
          <Route path="home" element={<Home />} />
          <Route path="account" element={<AccountSetting />} />
          <Route path="*" element={<Error />} />
        </Routes>
 */
