import React from "react";
import "./Category.css";
import SingleCategory from "../SingleCategory";
import data from "./../../data/category.json";

type CategoryProps = {
  chooseCategory: (id: number) => void;
};

const Category: React.FC<CategoryProps> = ({ chooseCategory }) => {
  
  return (
    <div className="category-wrapper">
      {data.map((item) => (
        <SingleCategory
          key={item.id}
          category={item}
          chooseCategory={chooseCategory}
        />
      ))}
    </div>
  );
};

export default Category;
