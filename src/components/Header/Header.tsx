import React from "react";
import Logo from "../Logo";
import Modal from "../Modal";
import Navbar from "../Navbar";
import Input from "../ui/Input";
import "./Header.css";

type HeaderProps = {
  modalInfo: { name: string; id: number; count: number }[];
  setIsOpen: React.Dispatch<React.SetStateAction<boolean>>;
  isOpen: boolean;
  img: string;
};
const Header: React.FC<HeaderProps> = ({
  modalInfo,
  isOpen,
  setIsOpen,
  img,
}) => {
  return (
    <>
      <header>
        <div className="header-input">
          <Logo />
          <Input placeholder="Search" label="" type="text" />
        </div>

        <Navbar
          modalInfo={modalInfo}
          isOpen={isOpen}
          setIsOpen={setIsOpen}
          img={img}
        />
      </header>
      {isOpen && (
        <Modal setIsOpen={setIsOpen} isOpen={isOpen} modalInfo={modalInfo} />
      )}
    </>
  );
};

export default Header;
