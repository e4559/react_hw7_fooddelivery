import React, { useState, useContext } from "react";
import Logo from "../Logo";
import Button from "../ui/Button";
import Checkbox from "../ui/Checkbox";
import Input from "../ui/Input";
import "./LoginForm.css";
import { UserContext } from "../App/App";

const LoginForm: React.FC = () => {
  const [inputInfo, setInputInfo] = useState({ email: "", password: "" });

  const x = useContext(UserContext);

  const handleEmail = (e: string) => {
    setInputInfo({ ...inputInfo, email: e });
  };
  const handlePass = (e: string) => {
    setInputInfo({ ...inputInfo, password: e });
  };

  const handleClick = () => {
    x?.handleLogin(inputInfo);
  };

  return (
    <>
      <div className="loginForm-wrapper">
        <Logo />

        <div className="form-wrapper">
          <div>
            {x?.errorMessage ? (
              <>
                <h3>{x?.errorMessage}</h3>
                <p>Try again with valid email and password</p>
              </>
            ) : (
              <>
                <h2>Login</h2>
                <p>
                  Sign in with your data that you entered during your
                  registration.
                </p>
              </>
            )}
          </div>

          <Input
            type="email"
            label="Email"
            placeholder="name@example.com"
            onChange={handleEmail}
          />
          <Input
            type="password"
            label="Password"
            placeholder="min. 8 characters"
            onChange={handlePass}
          />
          <Checkbox label="Keep me logged in" checked={false} />
          <Button name="Login" cn="login-btn" onClick={handleClick} />

          <Button name="Forgot password" cn="pass-btn" />

          <p className="signup">
            Dont have an account? <a href="google/com">Sign up</a>
          </p>
        </div>
      </div>
    </>
  );
};

export default LoginForm;
