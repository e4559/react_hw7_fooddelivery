import React from "react";
import "./Logo.css";

const Logo = () => {
  return (
    <div className="logo-wrapper">
      <p> Food</p>
      <p>delivery</p>
    </div>
  );
};

export default Logo;
