import React, { useContext, useRef } from "react";
import "./Modal.css";
import useCloseModal from "../../hooks/useCloseModal";
import { UserContext } from "../App/App";
type ModalProps = {
  modalInfo: { name: string; id: number; count: number }[];
  setIsOpen: React.Dispatch<React.SetStateAction<boolean>>;
  isOpen: boolean;
};
const Modal: React.FC<ModalProps> = ({ modalInfo, isOpen, setIsOpen }) => {
  const ref = useRef(null);
  const context = useContext(UserContext);
  useCloseModal(ref, () => setIsOpen(!isOpen));
  return (
    <div className="modal" ref={ref}>
      {modalInfo.length ? (
        modalInfo.map((item, i) => {
          return (
            <div
              onClick={() => context?.deleteBasketItem(item.id)}
              className="modal-item"
              key={i}
              role="presentation"
            >
              <p>{item.name}</p>
              <p>{item.count}</p>
              <p>X</p>
            </div>
          );
        })
      ) : (
        <p>Basket is empty</p>
      )}
    </div>
  );
};

export default Modal;
