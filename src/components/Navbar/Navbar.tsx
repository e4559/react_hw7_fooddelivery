import React from "react";
import basket from "./../../assets/basket.png";
import { NavLink } from "react-router-dom";
import "./Navbar.css";

type NavBarProps = {
  modalInfo: { name: string; id: number; count: number }[];
  setIsOpen: React.Dispatch<React.SetStateAction<boolean>>;
  isOpen: boolean;
  img: string;
};
const Navbar: React.FC<NavBarProps> = ({
  modalInfo,
  isOpen,
  setIsOpen,
  img,
}) => {
  let sum = 0;
  modalInfo.map((item) => (sum += item.count));
  return (
    <div className="header-right-item">
      <nav>
        <ul>
          <li>
            <NavLink to="/home">Restaurant</NavLink>
          </li>
          <li className="line">
            <NavLink to="/deals">Deals</NavLink>
          </li>
          <li
            className="orders"
            onClick={() => setIsOpen(!isOpen)}
            role="presentation"
            onKeyDown={() => setIsOpen(!isOpen)}
          >
            My Orders
            <div className="basket-wrapper">
              <img src={basket} alt="basket icon" />
              <div className="sup">{modalInfo && sum}</div>
            </div>
          </li>

          <li>
            <NavLink to="/account">
              <img className="profile-wrapper" src={img} alt="profile icon" />
            </NavLink>
          </li>
        </ul>
      </nav>
    </div>
  );
};

export default Navbar;

/*
<div class="header-right-item">
        <nav>
          <ul>
            <li><a href="#">Restaurant</a></li>
            <li><a href="#">Deals</a></li>
            <li><a href="#">My orders</a></li>
          </ul>
        </nav>

        <div class="basket-wrapper">
          <img src="./assets/basket.png" alt="basket icon" />
          <div class="sup">4</div>
        </div>
        <img
          class="profile-wrapper"
          src="./assets/img.png"
          alt="profile photo"
        />

        <div class="media-container">
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
 */
