import React, { useContext } from "react";
import "./Restaurants.css";
import SingleRestaurant from "./../SingleRestaurant/SingleRestaurant";
import restaurantData from "./../../data/restaurant.json";
import { UserContext } from "../../components/App/App";

type RestaurantsProps = {
  categoryId: number[];
};

type x = number[];
const Restaurants: React.FC<RestaurantsProps> = ({ categoryId }) => {
  const context = useContext(UserContext);

  const filtered: x = [];

  restaurantData.map((rest) => {
    rest.category.map((item) => {
      if (categoryId.includes(item.id) && !filtered.includes(rest.id)) {
        filtered.push(rest.id);
      }
    });
  });

  return (
    <div>
      <p>Nearby restaurants</p>
      <div className="restaurant-wrapper">
        {restaurantData.map((item, i) => {
          const rest = context?.modalInfo.filter((el) => el.id === item.id);
          if (categoryId.length == 0 && rest) {
            return <SingleRestaurant key={i} restaurant={item} rest={rest} />;
          } else if (filtered.includes(item.id) && rest) {
            return <SingleRestaurant key={i} restaurant={item} rest={rest} />;
          }
        })}
      </div>
    </div>
  );
};

export default Restaurants;

/*

return (
    <div>
      <p>Nearby restaurants</p>
      <div className="restaurant-wrapper">
        {categoryId.length == 0
          ? restaurantData.map((item, i) => {
              const rest = context?.modalInfo.filter((el) => el.id == item.id);
              console.log(rest, "abc");

              return <SingleRestaurant key={i} restaurant={item} />;
            })
          : restaurantData.map(
              (item, i) =>
                filtered.includes(item.id) && (
                  <SingleRestaurant key={i} restaurant={item} />
                )
            )}
      </div>
    </div>
  );



*/

/*
<div>
      <p>Nearby restaurants</p>
      <div className="restaurant-wrapper">
        {categoryId.length == 0
          ? restaurantData.map((item, i) => (
              <SingleRestaurant key={i} restaurant={item} />
            ))
          : restaurantData.map(
              (item, i) =>
                filtered.includes(item.id) && (
                  <SingleRestaurant key={i} restaurant={item} />
                )
            )}
      </div>
    </div>


*/
