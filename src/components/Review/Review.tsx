import React from "react";
import "./Review.css";
import img1 from "./../../assets/box3.png";
import img2 from "./../../assets/box.png";
import img3 from "./../../assets/box1.png";

const Review = () => {
  return (
    <div className="reviews-wrapper">
      <div className="img-container">
        <div className="img1">
          <img src={img1} alt="" />
        </div>
        <div className="img2">
          <img src={img2} alt="" />
        </div>
        <div className="img3">
          <img src={img3} alt="" />
        </div>
      </div>
      <div className="review-info">
        <h3>Leave reviews for all meals</h3>
        <p>
          Lorem ipsum dolor sit amet, magna scaevola his ei. Cum te paulo
          probatus molestiae, eirmod assentior eum ne, et omnis antiopam mel.
        </p>
        <div className="slider">
          <span></span>
          <span></span>
          <span></span>
          <span></span>
        </div>
      </div>
    </div>
  );
};

export default Review;
