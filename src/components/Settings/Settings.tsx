import React from "react";
import "./Settings.css";
import settings from "./../../data/settings.json";
import SingleSetting from "../SingleSetting";

type SettingsProps = {
  
  state: {
    1: { status: boolean };
    2: { status: boolean };
    3: { status: boolean };
    4: { status: boolean };
  };
  handleState: (id: number) => void;
};
const Settings: React.FC<SettingsProps> = ({ state, handleState }) => {
  return (
    <div className="settings">
      <p>Settings</p>
      <div className="settings-items-wrapper">
        {settings.map((item) => (
          <SingleSetting
            key={item.id}
            setting={item}
            handleState={handleState}
            status={(state as any)[item.id].status}
          />
        ))}
      </div>
    </div>
  );
};

export default Settings;
