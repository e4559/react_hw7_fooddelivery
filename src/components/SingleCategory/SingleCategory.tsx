import React, { useState } from "react";
import "./SingleCategory.css";

type SingleCategoryProps = {
  category: {
    img: string;
    id: number;
    name: string;
  };
  chooseCategory: (id: number) => void;
};

const SingleCategory: React.FC<SingleCategoryProps> = ({
  category,
  chooseCategory,
}) => {
  const [isActive, setIsActive] = useState(false);
  const handleActive = () => {
    setIsActive(!isActive);
    chooseCategory(category.id);
  };

  return (
    <div
      onClick={handleActive}
      className={`single-category-wrapper ${isActive && "activeClass"}`}
      aria-hidden="true"
    >
      <img src={category.img} alt="" />
      <p>{category.name}</p>
    </div>
  );
};

export default SingleCategory;
