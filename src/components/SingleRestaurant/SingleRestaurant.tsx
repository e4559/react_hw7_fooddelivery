import React, { useState, useContext } from "react";
import "./SingleRestaurant.css";
import bag from "./../../assets/colorlessbag.png";
import clock from "./../../assets/clock.png";
import point from "./../../assets/point.png";
import { UserContext } from "../../components/App/App";
import basket from "./../../assets/basket.png";
type SingleRestaurantProps = {
  restaurant: {
    id: number;
    img: string;
    name: string;
    time: string;
    sum: number;
    category: {
      name: string;
      img: string;
    }[];
  };
  rest: { name: string; id: number; count: number }[];
};

const SingleRestaurant: React.FC<SingleRestaurantProps> = ({
  restaurant,
  rest,
}): JSX.Element => {
  const context = useContext(UserContext);
  const [feature, setFeature] = useState(false);

  return (
    <div className="single-restaurant-wrapper">
      <div className="restImage">
        <img src={restaurant.img} alt="" />
      </div>

      <div
        onClick={() => setFeature(!feature)}
        role="presentation"
        className={`featured ${feature && "feature-active"}`}
      >
        <p>FEATURED</p>
      </div>

      <div className="rest-name-wrapper">
        <h2>{restaurant.name}</h2>
        <div
          onClick={() => context?.addToBasket1(restaurant.id, restaurant.name)}
          role="presentation"
        >
          {rest[0]?.count ? (
            <div className="bag-wrapper  ">
              <img src={basket} alt="basket icon" />
              <div className="sup">{rest[0].count}</div>
            </div>
          ) : (
            <img src={bag} alt="" />
          )}
        </div>
      </div>

      <div className="time-wrapper">
        <img className="clock" src={clock} alt="" />
        <span>{restaurant.time} min</span>
        <img className="point" src={point} alt="" />
        <span>${restaurant.sum} min sum</span>
      </div>
      <div className="category">
        {restaurant.category.map((item, i) => {
          return (
            <div key={i}>
              <img src={item.img} alt="" />
              <span>{item.name}</span>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default SingleRestaurant;
