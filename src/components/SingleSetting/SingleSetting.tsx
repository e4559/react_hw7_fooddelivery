import React from "react";
import "./SingleSetting.css";

type SingleSettingProps = {
  setting: { id: number; img: string; name: string; info: string };
  handleState: (id: number) => void;
  status: boolean;
};
const SingleSetting: React.FC<SingleSettingProps> = ({
  setting,
  handleState,
  status,
}) => {
  return (
    <div
      className={`settings-item ${status && "settings-item-active"}`}
      onClick={() => handleState(setting.id)}
      aria-hidden="true"
    >
      <div className={`settings-img ${status && "settings-img-active"}`}>
        <img src={setting.img} alt="" />
      </div>
      <div className="settings-info">
        <h3>{setting.name}</h3>
        <p>{setting.info}</p>
      </div>
    </div>
  );
};

export default SingleSetting;
