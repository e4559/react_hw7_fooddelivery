import React from "react";
import "./Button.css";
import classNames from "classnames";

type ButtonProps = {
  name: string;
  cn: string;
  onClick?: () => void;
};

const Button: React.FC<ButtonProps> = ({ name, cn, onClick }) => {
  return (
    <button onClick={onClick} className={cn}>
      {name}
    </button>
  );
};

export default Button;
