import React from "react";
import "./Checkbox.css";
type InputProps = {
  checked: boolean;
  label: string;
};

const Input: React.FC<InputProps> = ({ checked, label }) => {
  return (
    <div className="checkbox-wrapper">
      <label className="checkbox-label">
        <input type="checkbox" defaultChecked={checked} />
        {label}
      </label>
    </div>
  );
};

export default Input;
