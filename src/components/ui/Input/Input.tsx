import React from "react";
import "./Input.css";
type InputProps = {
  type: string;
  placeholder: string;
  label: string;
  onChange?: (e: string) => void;
};

const Input: React.FC<InputProps> = ({
  type,
  placeholder,
  label,
  onChange,
}) => {
  return (
    <div className="input-wrapper">
      <label className="label">
        {type !== "checkbox" && label}
        <input
          type={type}
          placeholder={placeholder}
          onChange={(e) => onChange && onChange(e.target.value)}
        />
        {type === "checkbox" && label}
      </label>
    </div>
  );
};

export default Input;
