import React from "react";
import "./InputAccount.css";

type InputAccountProps = {
  type: string;
  placeholder?: string ;
  label: string;
  value?: string;
  ref1?: React.RefObject<HTMLInputElement>;
};

const InputAccount: React.FC<InputAccountProps> = ({
  type,
  placeholder,
  label,
  value,
  ref1,
}) => {
  return (
    <div className="InputAccount-wrapper">
      <label>
        {label}
        <input
          type={type}
          value={value}
          ref={ref1}
          placeholder={String(placeholder)}
        />
      </label>
    </div>
  );
};

export default InputAccount;
