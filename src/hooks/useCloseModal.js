import React, { useEffect } from "react";

const useCloseModal = (ref, cb) => {
  const handleModal = (e) => {
    console.log(e, "e");
    if (!ref.current.contains(e.target)) {
      cb();
    }
  };

  useEffect(() => {
    if (ref.current) {
      window.addEventListener("click", handleModal);
    }
    return () => window.removeEventListener("click", handleModal);
  }, [ref]);
};

export default useCloseModal;
