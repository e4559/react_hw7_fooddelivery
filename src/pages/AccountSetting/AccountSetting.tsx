import React, { useState } from "react";
import Account from "../../components/Account";
import Address from "../../components/Address";
import Payment from "../../components/Payment";
import Security from "../../components/Security";
import Settings from "../../components/Settings";
import "./AccountSetting.css";

const AccountSetting = () => {
  const [state, setState] = useState({
    1: { status: true },
    2: { status: false },
    3: { status: false },
    4: { status: false },
  });

  const handleState = (id: number) => {
    Object.values(state).map((item) => {
      item.status = false;
    });
    setState({ ...state, [id]: { status: true } });
  };

  return (
    <div className="account-wrapper">
      <div className="account-container">
        <Settings state={state} handleState={handleState} />
        {state["1"].status && <Account />}
        {state["2"].status && <Address />}
        {state["3"].status && <Payment />}
        {state["4"].status && <Security />}
      </div>
    </div>
  );
};

export default AccountSetting;
