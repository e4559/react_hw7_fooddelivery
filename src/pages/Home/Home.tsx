import React, { useState } from "react";
import Ad from "../../components/Ad";
import Category from "../../components/Category";
import burger from "./../../assets/burger.png";
import cake from "./../../assets/cake.png";
import Restaurants from "../../components/Restaurants";
import "./Home.css";

const Home = () => {
  const [categoryId, setcategoryId] = useState([] as number[]);

  const chooseCategory = (id: number) => {
    if (!categoryId.includes(id)) {
      setcategoryId([...categoryId, id]);
    } else if (categoryId.includes(id)) {
      setcategoryId(categoryId.filter((el) => el !== id));
    }
  };

  return (
    <div className="home-wrapper">
      <div className="ads-wrapper">
        <Ad img={cake} cn="cake-cn">
          <div className="ad-children-wrapper">
            <p>All deserts</p>
            <h2 className="off20">20% OFF</h2>
            <p>Deserty</p>
          </div>
        </Ad>
        <Ad img={burger} cn="burger-cn">
          <div className="ad-children-wrapper">
            <p>Big Burgers</p>
            <h2 className="off50">50% OFF</h2>
            <p>Foodies</p>
          </div>
        </Ad>
      </div>
      <Category chooseCategory={chooseCategory} />
      <Restaurants categoryId={categoryId} />
    </div>
  );
};

export default Home;
