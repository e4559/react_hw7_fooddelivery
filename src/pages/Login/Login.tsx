import React from "react";
import LoginForm from "../../components/LoginForm";
import Review from "../../components/Review";
import "./Login.css";

const Login: React.FC = () => {
  return (
    <div className="login-container">
      <LoginForm />
      <Review />
    </div>
  );
};

export default Login;
