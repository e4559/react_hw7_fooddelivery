import React, { useContext, useEffect, useReducer } from "react";
import categoryData from "../data/category.json";
import restaurantData from "../data/restaurant.json";
import settingData from "../data/settings.json";
import userData from "../data/users.json";

// type ActionType = {
//   type: string;
//   modalInfo: { name: string; id: number; count: number };
// };

// type toggleModal = {
//   type: string;
// };
type addBasket = {
  type: string;
  modalInfo?: { name: string; id: number; count: number };
};

// type info = { name: string; id: number; count: number };

type ActionType = addBasket ;
export type StateType = {
  category: { id: number; name: string; img: string }[];
  restaurants: {
    id: number;
    img: string;
    name: string;
    time: string;
    sum: number;
    category?: { id: number; name: string; img: string }[];
  }[];
  users?: { email: string; password: string }[];
  settings: {
    id: number;
    img: string;
    name: string;
    info: string;
  }[];
  isOpen: boolean;
  modalInfo: { name: string; id: number; count: number }[];
};
type UserContextType = {
  state: StateType;
  dispatch: React.Dispatch<ActionType>;
};
type ProviderType = {
  children: React.ReactNode;
};

const ACTION_TYPES = {
  SET_DATA: "SET_DATA",
  TOGGLE_MODAL: "TOGGLE_MODAL",
  ADD_BASKET: "ADD_BASKET",
};

const defaultState: StateType = {
  category: [],
  restaurants: [],
  settings: [],
  modalInfo: [],
  isOpen: false,
};

function reducer(state: StateType, action: ActionType) {
  switch (action.type) {
    case ACTION_TYPES.TOGGLE_MODAL: {
      return { ...state, isOpen: !state.isOpen };
    }
    case ACTION_TYPES.SET_DATA: {
      return {
        ...state,
        category: categoryData,
        restaurants: restaurantData,
        users: userData,
        settings: settingData,
      };
    }

    case ACTION_TYPES.ADD_BASKET: {
      console.log(action, "hi");
      // let includes = false;
      if (!state.modalInfo.length) {
        return { ...state, modalInfo: action.modalInfo };
      }
      // state.modalInfo.map((item, i) => {
      //   if (item.id == action.modalInfo1?.id) {
      //     item.count += 1;
      //     includes = true;
      //   }
      //   return { ...state, modalInfo: state.modalInfo };
      // });
      // if (!includes) {
      //   return {
      //     ...state,
      //     modalInfo: [
      //       ...state.modalInfo,
      //       {
      //         id: action.modalInfo?.id,
      //         name: action.modalInfo?.name,
      //         count: action.modalInfo?.count,
      //       },
      //     ],
      //   };
      // }
    }

    default:
      return state;
  }
}

const Context = React.createContext<UserContextType | null>(null);

const useCustomContext = () => useContext(Context);

function ContextProvider({ children }: ProviderType) {
  const [state, dispatch] = useReducer(reducer, defaultState);

  useEffect(() => {
    dispatch({ type: ACTION_TYPES.SET_DATA });
  }, []);

  return (
    <Context.Provider value={{ state, dispatch }}>{children}</Context.Provider>
  );
}

export { ACTION_TYPES, ContextProvider, useCustomContext };

/*

import React, { useContext, useEffect, useReducer } from "react";
import categoryData from "../data/category.json";
import restaurantData from "../data/restaurant.json";
import settingData from "../data/settings.json";
import userData from "../data/users.json";

type ActionType = {
  type: string;
  modalInfo: { name: string; id: number; count: number };
};

export type StateType = {
  category: { id: number; name: string; img: string }[];
  restaurants: {
    id: number;
    img: string;
    name: string;
    time: string;
    sum: number;
    category?: { id: number; name: string; img: string }[];
  }[];
  users: { email: string; password: string }[];
  settings: {
    id: number;
    img: string;
    name: string;
    info: string;
  }[];
  isOpen: boolean;
  modalInfo: { name: string; id: number; count: number }[];
};
type UserContextType = {
  state: StateType;
  dispatch: React.Dispatch<ActionType>;
};
type ProviderType = {
  children: React.ReactNode;
};

const ACTION_TYPES = {
  SET_CATEGORY: "SET_CATEGORY",
  SET_DATA: "SET_DATA",
  TOGGLE_MODAL: "TOGGLE_MODAL",
  ADD_BASKET: "ADD_BASKET",
};

const defaultState = {
  category: [],
  restaurants: [],
  settings: [],
  modalInfo: [],
  isOpen: false,
};

function reducer(state: StateType, action: ActionType) {
  switch (action.type) {
    case ACTION_TYPES.TOGGLE_MODAL: {
      return { ...state, isOpen: !state.isOpen };
    }
    // case ACTION_TYPES.SET_DATA: {
    //   return {
    //     ...state,
    //     category: categoryData,
    //     restaurants: restaurantData,
    //     users: userData,
    //     settings: settingData,
    //   };
    // }

    // case ACTION_TYPES.ADD_BASKET: {
    //   let includes = false
    // if (!state.modalInfo.length) {
    //   return { ...state, modalInfo: action.modalInfo };
    // }
    // state.modalInfo.map((item, i) => {
    //       if (item.id == action.modalInfo.id) {
    //         item.count += 1;
    //         includes = true;
    //       }
    //       return {...state, modalInfo: state.modalInfo}

    //     });
    // if (!includes) {
    //   return { ...state, modalInfo: [...state.modalInfo, {id:action.modalInfo.id, name:action.modalInfo.name, count :action.modalInfo.count}]}
    // }
    // };
  }
  default:
    return state;
}

const Context = React.createContext<UserContextType | null>(null);

const useCustomContext = () => useContext(Context);

function ContextProvider({ children }: ProviderType) {
  const [state, dispatch] = useReducer(reducer, defaultState);

  useEffect(() => {
    dispatch({ type: ACTION_TYPES.SET_DATA });
  }, []);

  return (
    <Context.Provider value={{ state, dispatch }}>{children}</Context.Provider>
  );
}

export { ACTION_TY




*/
